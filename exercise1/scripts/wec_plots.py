#!/bin/python

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

source_location = "../../exercise0/WECs_DataSet"
output_location = "../plots"

perth = "Perth_Data.csv"
adelaide = "Adelaide_Data.csv"
sydney = "Sydney_Data.csv"
tasmania ="Tasmania_Data.csv"
datasets = [perth,adelaide,sydney,tasmania]

column_headers = []

for j in ["X","Y","P"]:
    for i in list(range(1,17)):
        column_headers.append(j+str(i))

column_headers.append("P")


dataset = pd.DataFrame()
for d in datasets:
    location = pd.read_csv(source_location+"/"+d,sep=',',names=column_headers)

    dataset = pd.concat([dataset,location],axis=0)


plot_title = "all_locations_x1"
f=plt.figure(plot_title)
sns.distplot(
    dataset['X1'], kde=False, hist=True, hist_kws={"label":plot_title}
).set(xlabel='X positions',ylabel="count",title=plot_title)

f.savefig(output_location+"/wec_"+plot_title)

plot_title = "all_locations_y1"
f=plt.figure(plot_title)
sns.distplot(
    dataset['Y1'], kde=False, hist=True, hist_kws={"label":plot_title}
).set(xlabel='Y positions',ylabel="count",title=plot_title)

f.savefig(output_location+"/wec_"+plot_title)


plot_title = "all_locations_power_output"
f=plt.figure(plot_title)
sns.distplot(
    dataset['P'], kde=False, hist=True, hist_kws={"label":plot_title}
).set(xlabel='Total power output',ylabel="count",title=plot_title)

f.savefig(output_location+"/wec_"+plot_title)
