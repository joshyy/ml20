#!/usr/bin/python3

import wec

import argparse
from ast import literal_eval
import math
import numpy as np
import pandas
from scipy import stats
import seaborn
import matplotlib.pyplot as pyplot
from sklearn import svm, tree, linear_model, neural_network, model_selection, preprocessing
from statistics import mean, stdev

scoring = []


def try_parse(s):
    try:
        return literal_eval(s)
    except:
        return s


parser = argparse.ArgumentParser(description="Draw score of a regression method depending on a\
        parameter for the WEC dataset.")
parser.add_argument("-r", action="store", metavar="Reg", default="dt",
                    choices=["dt", "bayes", "svm", "mlp"],
                    help="Select the regressor. Possiblitites are:\
                'dt' (Decision Tree, default),\
                'bayes' (Bayesian Regressor),\
                'svm' (Support Vector Machine),\
                'mlp' (Multilayer Perceptron)")
parser.add_argument("--no-scaling", action="store_true")
parser.add_argument("--rounding", action="store_true")
parser.add_argument("--keep-outliers", action="store_true")
parser.add_argument("--use-all-features",action="store_true")
parser.add_argument("-f", action="store", type=int, metavar="Folds", default=10,
                    help="Number of folds")
parser.add_argument("-s", action="store", metavar="Scoring",
                    choices=["explained_variance", "max_error", "neg_mean_absolute_error",
                             "neg_mean_squared_error", "neg_root_mean_squared_error",
                             "neg_mean_squared_log_error", "neg_median_absolute_error",
                             "r2", "neg_mean_poisson_deviance", "neg_mean_gamma_deviance"],
                    help="Type of scoring used")
parser.add_argument("--params", action="append", metavar="id=val", default=[],
                    help="A parameter that should be passed to the regressor")



def main():
    global HEADERS, scoring

    args = parser.parse_args()

    regressor_id = args.r
    print(regressor_id)
    do_scaling = not args.no_scaling
    do_rounding = args.rounding
    remove_outliers = not args.keep_outliers
    use_pk = args.use_all_features
    folds = args.f
    if args.s != None:
        scoring.append(args.s)
    else:
        scoring = ['neg_root_mean_squared_error']
    params = {}
    for kv in args.params:
        [key, value] = kv.split("=", 1)
        params[key] = try_parse(value)

    normalised = wec.prepare_data(wec.datasets
                    ,do_rounding=do_rounding
                    ,do_scaling=do_scaling
                    ,remove_outliers=remove_outliers)


    # regressors = {"dt": (tree.DecisionTreeRegressor,{"min_samples_leaf":100})
    #              ,"br": (linear_model.BayesianRidge,{"n_iter": 500,"tol":1e-5})
    #              ,"svm": (svm.LinearSVR,{"loss":"squared_epsilon_insensitive","intercept_scaling":50,"dual":False})
    #              ,"mlp": (neural_network.MLPRegressor,{})}
    #
    # # regressor_id = "mlp"
    # # folds = 10
    # # use_pk = False
    # # scoring = ["neg_root_mean_squared_error" ,"neg_mean_absolute_error"]
    _features, _target = wec.split(normalised["all"],use_pk)

    regressor = None
    if regressor_id == "dt":
        regressor = tree.DecisionTreeRegressor
    elif regressor_id == "bayes":
        regressor = linear_model.BayesianRidge
    elif regressor_id == "svm":
        regressor = svm.LinearSVR
    elif regressor_id == "mlp":
        regressor = neural_network.MLPRegressor

    score = model_selection.cross_validate(regressor(**params),
                                           _features, y=_target, cv=folds, scoring=scoring
                                           ,return_train_score=True)

    # for k,(regressor,params) in regressors.items():
    #     score = model_selection.cross_validate(regressor(**params),
    #                                            _features, y=_target, cv=folds, scoring=scoring
    #                                            ,return_train_score=True)

        # if create_plot:
        #     print("plot")

    if score is not None:
        for sc in scoring:
            s = score["test_" + sc]
            print("%s: %.4f +- %.2f" % (sc, mean(s), stdev(s)))


if __name__== "__main__":
    main()
