#!/usr/bin/python3

from datetime import datetime

import pandas
from sklearn import preprocessing

### Preprocessing of the traffic dataset ######################################
def preprocess(dataset, do_scaling=True, use_weekday=True, restrict=False):
    dataset = dataset.copy()

    # Filter out outliers
    # Temperature: For some entries the temperature is 0
    # Rain [mm/h]: One entry has >9000 mm/h
    dataset = dataset[(dataset["temp"] > 0) & (dataset["rain_1h"] < 100)]


    # split datetime into four or five distinct features
    # (year, month, day, hour[, day of the week])
    date_time = [datetime.strptime(dt, "%Y-%m-%d %H:%M:%S")
                for dt in dataset["date_time"]]
    year = [dt.year for dt in date_time]
    month = [dt.month for dt in date_time]
    day = [dt.day for dt in date_time]
    hour = [dt.hour for dt in date_time]
    weekday = [dt.strftime("%A") for dt in date_time]

    del dataset["date_time"]
    dataset.insert(7, "year", year)
    dataset.insert(8, "month", month)
    dataset.insert(9, "hour", hour)
    dataset.insert(10, "weekday", weekday)


    # lower strings, so classes are case insensitive
    dataset["holiday"] = [w.lower() for w in dataset["holiday"]]
    dataset["weather_main"] = [w.lower() for w in dataset["weather_main"]]
    dataset["weather_description"] = \
        [w.lower() for w in dataset["weather_description"]]
    dataset["weekday"] = [w.lower() for w in dataset["weekday"]]


    # Scaling
    if do_scaling:
        numerical = ["temp", "rain_1h", "snow_1h", "clouds_all"]
        target_id = "traffic_volume"
        for feature in numerical + [target_id]:
            dataset[feature] = preprocessing.scale(dataset[feature],
                    with_mean=True, with_std=True, copy=True)

    # Restrict number of features
    if restrict:
        features_restrict = ["weather_description", "year", "rain_1h"]
    else:
        features_restrict = []
    if not use_weekday:
        features_restrict += ["weekday"]
    for f in features_restrict:
        del dataset[f]
    if restrict:
        dataset["holiday"] = ["yes" if h == "None" else "no"
                              for h in dataset["holiday"]]

    # one-hot encoding
    categorical = list(set(["holiday", "weather_main", "weather_description",
        "year", "month", "hour", "weekday"]) - set(features_restrict))
    dataset = pandas.get_dummies(dataset, columns=categorical, drop_first=False)

    # Seperate features and target
    features = dataset.drop("traffic_volume", axis=1)
    target = dataset["traffic_volume"]

    return (features, target)
