#!/usr/bin/env python3

import numpy
import pandas
import seaborn
from matplotlib import pyplot

import news

def plotHeatMap(currentDataset, figurePath, title):
    corrmat = currentDataset.corr()
    heatmap = seaborn.heatmap(currentDataset[corrmat.index].corr(), annot=True)
    heatmap.set(title=title)
    pyplot.tight_layout()
    pyplot.savefig(figurePath)
    pyplot.clf()

pyplot.rcParams["figure.figsize"] = (10, 6)

# read dataset
dataset_path = "../datasets/online_news_popularity.csv"
dataset = pandas.read_csv(dataset_path, sep=",\s+")

# print (continuous) target attribute including threshold
pyplot.hist(dataset['shares'], bins=numpy.arange(0, 8100, 100))
pyplot.axvline(x=1400, color='r', linewidth=1)
pyplot.xlabel('Number of shares')
pyplot.ylabel('Number of articles per shares')
pyplot.title('Shares for articles')
pyplot.tight_layout()
pyplot.savefig("../plots/popularity_shares_dist.png")
pyplot.clf()

numOfFeatures = 3
arguments = {"numOfFeatures": numOfFeatures }

# produce plot of 10 most important features
(features, target) = news.preprocess(dataset, arguments, False)
plotHeatMap(pandas.concat([features, target], axis=1), "../plots/popularity-heatmap.png", str(numOfFeatures) + " most important features")

# remove 40 most important features from the dataset and make the preprocess again
# plot the heatmap again
datasetOtherFeatures = dataset.drop(features.columns, axis=1)
arguments["numOfFeatures"] = 30
(features, target) = news.preprocess(datasetOtherFeatures, arguments, False)
datasetOtherFeatures = datasetOtherFeatures.drop(features.columns, axis=1)

arguments["numOfFeatures"] = numOfFeatures
(features, target) = news.preprocess(datasetOtherFeatures, arguments, False)
plotHeatMap(pandas.concat([features, target], axis=1), "../plots/popularity-heatmap-other-features.png", "10 most important features after removing " + str(30 + numOfFeatures))
