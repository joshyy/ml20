#!/usr/bin/python3

import numpy as np
import pandas
import seaborn
import matplotlib.pyplot as pyplot

### Settings ##################################################################
pyplot.rcParams["figure.figsize"] = (10, 6)


### Load dataset ##############################################################
dataset_path = "../datasets/real_estate_valuation.csv"
dataset = pandas.read_csv(dataset_path, sep=",")


### Plotting ##################################################################

# Age Distribution
seaborn.distplot(dataset["house_age"], kde=False, norm_hist=False).set(
        xlabel="House Age [a]", ylabel="Count", title="House Age distribution")
pyplot.tight_layout()
pyplot.savefig("../plots/estate-age.png")
pyplot.clf()

# MRT Distance Distribution
seaborn.distplot(dataset["mrt_distance"], kde=False, norm_hist=False).set(
        xlabel="MRT Distance [m]", ylabel="Count",
        title="Distribution of the MRT Distance")
pyplot.tight_layout()
pyplot.savefig("../plots/estate-mrt_distance.png")
pyplot.clf()

# Price Distribution
seaborn.distplot(dataset["price_per_area"], kde=False, norm_hist=False).set(
        xlabel="Price per area [10000 New Taiwan Dollar/Ping]", ylabel="Count",
        title="Price distribution")
pyplot.tight_layout()
pyplot.savefig("../plots/estate-price.png")
pyplot.clf()
