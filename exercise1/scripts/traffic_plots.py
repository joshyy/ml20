#!/usr/bin/python3

import numpy as np
import pandas
import seaborn
import matplotlib.pyplot as pyplot

### Settings ##################################################################
pyplot.rcParams["figure.figsize"] = (10, 6)


### Load dataset ##############################################################
dataset_path = "../datasets/metro_traffic_volume.csv"
dataset = pandas.read_csv(dataset_path, sep=",")


### Preprocess dataset ########################################################
dataset["weather_main"] = [w.lower() for w in dataset["weather_main"]]
dataset["weather_description"] = [w.lower() for w in dataset["weather_description"]]


### Plotting ##################################################################

# Temperature
# For some entries the temperature is 0
seaborn.distplot([t for t in dataset["temp"] if t > 0], kde=False, norm_hist=False).set(
        xlabel="Temperature [K]", ylabel="Count", title="Temperature distribution")
pyplot.tight_layout()
pyplot.savefig("../plots/traffic-temp.png")
pyplot.clf()

# Rain [mm/h]
# One entry has >9000 mm/h
seaborn.distplot([r for r in dataset["rain_1h"] if r > 0 and r < 3], kde=False, norm_hist=False).set(
        xlabel="Rain [mm/h]", ylabel="Count", title="Rain distribution")
pyplot.tight_layout()
pyplot.savefig("../plots/traffic-rain.png")
pyplot.clf()

# Snow [mm/h]
seaborn.distplot([s for s in dataset["snow_1h"] if s > 0], kde=False, norm_hist=False).set(
        xlabel="Snow [mm/h]", ylabel="Count", title="Snow distribution")
pyplot.tight_layout()
pyplot.savefig("../plots/traffic-snow.png")
pyplot.clf()

# Cloud cover [%]
seaborn.distplot(dataset["clouds_all"], kde=False, norm_hist=False).set(
        xlabel="Cloud cover [%]", ylabel="Count", title="Cloud cover distribution")
pyplot.tight_layout()
pyplot.savefig("../plots/traffic-clouds.png")
pyplot.clf()

# Wheater textual short
ax = seaborn.countplot(x="weather_main",
                       data=dataset,
                       order=dataset["weather_main"].value_counts().index)
ax.set(xlabel="Wheater description", ylabel="Count", title="Counts of short wheater description")
pyplot.tight_layout()
pyplot.savefig("../plots/traffic-weather_main.png")
pyplot.clf()

# Wheater textual long
ax = seaborn.countplot(x="weather_description",
                       data=dataset,
                       order=dataset["weather_description"].value_counts().index)
ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha="right")
ax.set(xlabel="Wheater description", ylabel="Count", title="Counts of long wheater description")
pyplot.tight_layout()
pyplot.savefig("../plots/traffic-weather_description.png")
pyplot.clf()

# Traffic volume
seaborn.distplot(dataset["traffic_volume"], kde=False, norm_hist=False).set(
        xlabel="Traffic volume [Persons]", ylabel="Count", title="Traffic volume distribution")
pyplot.tight_layout()
pyplot.savefig("../plots/traffic-traffic.png")
pyplot.clf()
