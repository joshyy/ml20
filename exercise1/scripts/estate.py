#!/usr/bin/python3

import math

import pandas
from sklearn import preprocessing

### Preprocessing of the estate dataset #######################################
def preprocess(dataset, do_scaling=True, extract_month=False):
    dataset = dataset.copy()

    target_id = "price_per_area"
    del dataset["no"]

    # Remove outliers
    dataset = dataset[dataset[target_id] < 90]

    # Extracting the year/month
    if extract_month:
        categorical = ["month", "year"]
        numerical = ["house_age", "mrt_distance", "n_convenience_stores",
                "latitude", "longitude"]
        months = [int(round((t % 1) * 12)) + 1 for t in dataset["transaction_date"]]
        years = [int(math.floor(t)) for t in dataset["transaction_date"]]
        del dataset["transaction_date"]
        dataset.insert(6, "year", years)
        dataset.insert(7, "month", months)
        dataset = pandas.get_dummies(dataset, columns=categorical, drop_first=False)
    else:
        categorical = []
        numerical = ["transaction_date", "house_age", "mrt_distance",
                "n_convenience_stores", "latitude", "longitude"]

    # Scaling
    if do_scaling:
        for feature in numerical + [target_id]:
            dataset[feature] = preprocessing.scale(dataset[feature],
                    with_mean=True, with_std=True, copy=True)

    # Seperate features and target
    features = dataset.drop("price_per_area", axis=1)
    target = dataset["price_per_area"]

    return (features, target)
