#!/usr/bin/python3

import pandas
from sklearn import model_selection, preprocessing

def crate_column_headers():

    column_headers = []

    for j in ["X","Y","P"]:
        for i in list(range(1,17)):
            column_headers.append(j+str(i))
            if j == "P":
                p_dim.append(j+str(i))

    column_headers.append("P")
    p_dim.append("P")
    return column_headers


def add_dataset(path):
    global HEADERS

    if HEADERS is None:
        HEADERS = crate_column_headers()

    name = path[path.rfind('/')+1:path.rfind('_')].lower()

    dataset = pandas.read_csv(path, sep=",",names=HEADERS)

    #sample data
    dataset = dataset.sample(frac=1)
    # dataset = dataset[:35000]

    datasets[name] = dataset


def remove_outlier(df_in, col_name):
    q1 = df_in[col_name].quantile(0.25)
    q3 = df_in[col_name].quantile(0.75)
    iqr = q3-q1 #Interquartile range
    fence_low  = q1-1.5*iqr
    fence_high = q3+1.5*iqr
    df_out = df_in.loc[(df_in[col_name] > fence_low) & (df_in[col_name] < fence_high)]
    return df_out


def preprocess(dataset,do_rounding=False,do_scaling=True,remove_outliers=True):

    if do_rounding:
        dataset = dataset.round(decimals=0)

    if do_scaling:
        for h in HEADERS:
            dataset[h] = preprocessing.scale(dataset[h]
                                                   ,with_mean=True, with_std=True,copy=True)

    # remove outliers
    if remove_outliers:
        dataset = remove_outlier(dataset,"P")

    return dataset.copy()


def evaluate(dataset,regressor,folds,scores,**params):

    _features, _target = dataset
    score = model_selection.cross_validate(regressor(**params), #
                                           _features, y=_target, cv=folds, scoring=scores
                                           ,return_train_score=True)

    return score


def split(dataset,use_pk=False):
    global p_dim

    if use_pk:
        features = dataset.drop(["P"],axis=1)
    else:
        features = dataset.drop(p_dim, axis=1)
    target = dataset["P"]

    return features,target


def prepare_data(data,do_rounding=False,do_scaling=True,remove_outliers=True):
    normalised = dict()

    for k,v in data.items():
        normalised[k] = preprocess(v.copy(),do_rounding,do_scaling,remove_outliers)

    allLocations = pandas.DataFrame()
    allButTasmania = pandas.DataFrame()
    for k,v in normalised.items():
        allLocations = pandas.concat([allLocations,v],axis=0)
        if k != "tasmania":
            allButTasmania = pandas.concat([allLocations,v],axis=0)

    normalised["all"] = allLocations
    normalised["all_no_tasmania"] = allButTasmania

    return normalised.copy()


datasets = dict()

p_dim = []

HEADERS = crate_column_headers()

add_dataset("../datasets/WECs_DataSet/Adelaide_Data.csv")
add_dataset("../datasets/WECs_DataSet/Perth_Data.csv")
add_dataset("../datasets/WECs_DataSet/Sydney_Data.csv")
add_dataset("../datasets/WECs_DataSet/Tasmania_Data.csv")

