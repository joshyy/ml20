#!/usr/bin/env python3

import pandas
from numpy import set_printoptions
from sklearn import preprocessing
from sklearn.feature_selection import SelectKBest, f_classif


def preprocess(dataset, arguments, doScaling = True):
    # convert shares continuous attribute into boolean
    dataset['shares'] = dataset['shares'].replace(dataset['shares'] >= 1400)

    # featureSelection
    X = dataset.copy()
    X = X.drop(['shares', 'url', 'timedelta'], axis=1)
    target = dataset['shares']
    # return (X, target)
    test = SelectKBest(score_func=f_classif, k=arguments['numOfFeatures'])
    fit = test.fit(X, target)
    set_printoptions(precision=3)
    selectedFeatures = X.columns[test.get_support(indices=True)]
    features = dataset[selectedFeatures]

    removeOutliers = True
    for column in features.columns:
        if doScaling:
            dataset[column] = preprocessing.scale(dataset[column])
        if removeOutliers:
            q1 = dataset[column].quantile(0.25)
            q3 = dataset[column].quantile(0.75)
            iqr = q3-q1
            fence_low = q1-1.5*iqr
            fence_high = q3+1.5*iqr
            dataset[column] = dataset[column].loc[(dataset[column] > fence_low) & (dataset[column] < fence_high)] 
    if doScaling:
        target = preprocessing.scale(target)

    return (pandas.DataFrame(features), pandas.DataFrame(target))
