#!/usr/bin/python3

import estate

import argparse
from ast import literal_eval

import pandas
import seaborn
import matplotlib.pyplot as pyplot
from sklearn import svm, tree, linear_model, neural_network, model_selection


def try_parse(s):
    try:
        return literal_eval(s)
    except:
        return s


parser = argparse.ArgumentParser(description="Draw score of a regression method depending on a\
        parameter for the estate dataset.")
parser.add_argument("-r", action="store", metavar="Reg", default="dt",
        choices=["dt", "bayes", "svm", "mlp"],
        help="Select the regressor. Possiblitites are:\
                'dt' (Decision Tree, default),\
                'bayes' (Bayesian Regressor),\
                'svm' (Support Vector Machine),\
                'mlp' (Multilayer Perceptron)")
parser.add_argument("-f", action="store", type=int, metavar="Folds", default=10,
        help="Number of folds")
parser.add_argument("-s", action="store", metavar="Scoring",
        choices=["explained_variance", "max_error", "neg_mean_absolute_error",
            "neg_mean_squared_error", "neg_root_mean_squared_error",
            "neg_mean_squared_log_error", "neg_median_absolute_error",
            "r2", "neg_mean_poisson_deviance", "neg_mean_gamma_deviance"],
        help="Type of scoring used")

parser.add_argument("-p", action="store", metavar="Path", default=None,
        help="Place to store the plot")
parser.add_argument("-t", action="store", metavar="Title", default="",
        help="Title of the plot")
parser.add_argument("--xscale", action="store", default="linear",
        choices=["linear", "log"],
        help="How to scale the x axis. Possibilites are: 'linear', 'log'")
parser.add_argument("--yscale", action="store", default="linear",
        choices=["linear", "log"],
        help="How to scale the y axis. Possibilites are: 'linear', 'log'")

parser.add_argument("param", action="store",
        help="The parameter, over which will be plotted")
parser.add_argument("values", nargs="+",
        help="A list of values to use for the given parameter")
parser.add_argument("--params", action="append", metavar="id=val", default=[],
        help="A parameter that should be passed to the regressor")


args = parser.parse_args()

regressor_id = args.r
folds = args.f
if args.s != None:
    scoring = args.s
else:
    scoring = 'neg_root_mean_squared_error'

path = args.p
title = args.t
xscale = args.xscale
yscale = args.yscale
param_id = args.param
param_values = [try_parse(s) for s in args.values]
params = {}
for kv in args.params:
    [key, value] = kv.split("=", 1)
    params[key] = try_parse(value)


### Load dataset ##############################################################
dataset_path = "../datasets/real_estate_valuation.csv"
dataset = pandas.read_csv(dataset_path, sep=",")
dataset = dataset.sample(frac=1)
(features, target) = estate.preprocess(dataset)

### Evaluation ################################################################

if regressor_id == "dt":
    regressor = tree.DecisionTreeRegressor
elif regressor_id == "bayes":
    regressor = linear_model.BayesianRidge
elif regressor_id == "svm":
    regressor = svm.LinearSVR
elif regressor_id == "mlp":
    regressor = neural_network.MLPRegressor

test_scores = []
train_scores = []
_values = []
for value in param_values:
    model = regressor(**params, **{param_id: value})
    score = model_selection.cross_validate(model, features, y=target,
            cv=folds, return_train_score=True, scoring = scoring)
    test_scores += score["test_score"].tolist()
    train_scores += score["train_score"].tolist()
    _values += [value] * folds

if all([type(v) is int or type(v) is float for v in _values]):
    seaborn.lineplot(_values, test_scores, err_style="band")
    axis = seaborn.lineplot(_values, train_scores, err_style="band")
    axis.set(xlabel=param_id, ylabel="Score", title=title)
    axis.set_xscale(xscale)
    axis.set_yscale(yscale)
    pyplot.tight_layout()
    pyplot.legend(["Test scores", "Training scores"])
else:
    _values = [str(v) for v in _values]
    data = pandas.DataFrame(data = {param_id: _values,
        "Test": test_scores, "Train": train_scores})
    data = pandas.melt(data, id_vars = param_id, var_name = "Train/Test", value_name = "Score")
    axis = seaborn.catplot(x = param_id, y = "Score", hue = "Train/Test", data = data)
    axis.set(title=title)
    pyplot.tight_layout(rect=[0,0,0.86,1])

if path==None:
    pyplot.show()
else:
    pyplot.savefig(path)
