#!/usr/bin/python3

import traffic

import argparse
from ast import literal_eval
from statistics import mean, stdev

import pandas
import seaborn
import matplotlib.pyplot as pyplot
from sklearn import svm, tree, linear_model, neural_network, model_selection


def try_parse(s):
    try:
        return literal_eval(s)
    except:
        return s


parser = argparse.ArgumentParser(description="Fit a regression model to the traffic dataset\
        and evaluate.")
parser.add_argument("-r", action="store", metavar="Reg", default="dt",
        choices=["dt", "bayes", "svm", "mlp"],
        help="Select the regressor. Possiblitites are:\
                'dt' (Decision Tree, default),\
                'bayes' (Bayesian Regressor),\
                'svm' (Support Vector Machine),\
                'mlp' (Multilayer Perceptron)")
parser.add_argument("--no-scaling", action="store_true")
parser.add_argument("--no-weekday", action="store_true")
parser.add_argument("--restrict", action="store_true")
parser.add_argument("-f", action="store", type=int, metavar="Folds", default=10,
        help="Number of folds")
parser.add_argument("-s", action="append", metavar="Scoring",
        choices=["explained_variance", "max_error", "neg_mean_absolute_error",
            "neg_mean_squared_error", "neg_root_mean_squared_error",
            "neg_mean_squared_log_error", "neg_median_absolute_error",
            "r2", "neg_mean_poisson_deviance", "neg_mean_gamma_deviance"],
        help="Type of scoring used")
parser.add_argument("--params", action="append", metavar="id=val", default=[],
        help="A parameter that should be passed to the regressor")


args = parser.parse_args()

regressor_id = args.r
do_scaling = not args.no_scaling
use_weekday = not args.no_weekday
restrict = args.restrict
folds = args.f
if args.s != None:
    scoring = args.s
else:
    scoring = ['neg_root_mean_squared_error']
params = {}
for kv in args.params:
    [key, value] = kv.split("=", 1)
    params[key] = try_parse(value)


### Load dataset ##############################################################
dataset_path = "../datasets/metro_traffic_volume.csv"
dataset = pandas.read_csv(dataset_path, sep=",")
dataset = dataset.sample(frac=1);
(features, target) = traffic.preprocess(dataset, do_scaling = do_scaling,
        use_weekday = use_weekday, restrict = restrict)

### Evaluation ################################################################

if regressor_id == "dt":
    regressor = tree.DecisionTreeRegressor
elif regressor_id == "bayes":
    regressor = linear_model.BayesianRidge
elif regressor_id == "svm":
    regressor = svm.LinearSVR
elif regressor_id == "mlp":
    regressor = neural_network.MLPRegressor

model = regressor(**params)
score = model_selection.cross_validate(model, features, y=target,
        cv=folds, scoring = scoring)
for s_id in scoring:
    s = score["test_" + s_id]
    print("%.2f +- %.2f" % (mean(s), stdev(s)))
