% Dataset Description


# Group Members

- 00725631, Edward Haupt
- 01426835, Nihad Abou-Zid
- 01526325, Johannes Varga

# Resources
[1] https://archive.ics.uci.edu/ml/datasets/statlog+(german+credit+data)

[2] http://archive.ics.uci.edu/ml/datasets/Wave+Energy+Converters 

[3] Neshat, M., Alexander, B., Wagner, M., & Xia, Y. (2018, July). A detailed comparison of meta-heuristic methods for optimising wave energy converter placements. In Proceedings of the Genetic and Evolutionary Computation Conference (pp. 1318-1325). ACM. 

# Dataset credit-g
This data set contains information about german citizens used to evaluate the credit rating. It dates back to 1994.
It consists of data collected by a bank about debtors and it includes data like job information, age, purpose of the credit and the personal status.

## Explanation of choice for data sets
- small sample size in contrary to second choice
- small number of attributes
- in contrast to the second choice this data set has also categorical attributes

## Characteristics of data set
- Number of instances: 1000
- Number of attributes: 20
- Missing values: No missing values, however some attributes have an unknown value,
  which could be used as missing value
- Area: financial
- Attribute characteristics: Categorical, Integer
- Date donated: 1994-11-17
- Associated tasks: classification
- Number of web hists: 588616

## How many samples, how many attributes
- samples: 1000
- attributes: 20

## What types of attributes (nominal, ordinal, interval, ...)
<!-- 2 data sub-sets are included: the original one 'german.data' consists of 7 numeric and 13 categorical attributes.
The second sub-set 'german.data-numeric' contains 24 numerical attributes.
It has been created for algorithms which cannot cope with categorical data.
In the analysis we only used the original data set. For the categorical attributes the value codes (like A14, A121, ...)
have been replaced by their respective description given in the documentation of the data set. -->

The data set has 20 attributes (without the target value).
Here is the characteristics of these attributes ([u] means the attribute has an unknown value):

- 12 of them are nominal
  * checking account status
  * credit history
  * [u] purpose
  * years employed
  * status/sex
  * guarentors
  * [u] property
  * installment plans
  * housing
  * job
  * telephone
  * foreign worker
- 1 of them is ordinal
  * [u] savings account status
- 7 of them are numerical
  * duration
  * credit amount
  * installment rate
  * resident since
  * age
  * number of credits
  * number of people being liable
- the target attribute is a two-valued nominal (good, bad)

All attributes are already well suited for machine learning and therefore no preprocessing is required.

## Distributions
The following four images represent the distribution of four different attributes (job, age, duration, checking account).
The visualization types produced are either a barplot or a histogram depending on the concrete attribute.
Barplot has been used for the numerical and countplot for the categorical data. The y-axis is always the number of citizens.

![Job Barplot](images/Job.png){width=10cm}

![Age Histogram](images/Age_in_years.png){width=10cm}

![Duration in month histogram](images/Duration_in_month.png){width=10cm}

![Status of existing checking account Barplot](images/Status_of_existing_checking_account.png){width=10cm}


<!-- #### Preprocessing
Was not used for this data set as we don't know about missing values. For the numerical attributes we deal with discrete data most
of the times leading to the fact that we don't have to round the values. Moreover the sample size is fairly small.
Therefore the calculations are not expensive.) -->


# Data set "Wave Energy Converters Data Set"

## Description

The data set consists of four subsets, each of them having the same structure.
These subsets are associated with locations in Australia
(Adelaide, Perth, Sydney, Tasmania).
The data describes roughly spoken the absorbed power of wave energy converters
when they are placed in different ways within a fixed region.

<!-- The data set consists of positions and absorbed power outputs of wave energy
converters (WECs) at four locations in Australia. On each location measurements
were taken at different positions resulting in as many absorbed power outputs
per sample. -->

## Explanation of choice for data sets

We chose this data set as a diverese counterpart to the data set "credit-g".
The two sets differ in sample size and attributes, both are a lot larger in
this set. Also there are missing values present in this data set.

## Characteristics of data set

- Multivariate
- Number of Instances: 288000
- Area: Computer
- Attribute Characteristics: Real
- Number of Attributes: 49
- Date Donated: 2019-06-30
- Associated Tasks: Regression
- Missing Values?: Yes
- Number of Web Hits: 14600

## How many samples,how many attributes

As mentioned above the data set consists of 288000 samples. These are split up
into four files, each file representing one location where measurements were
taken. The data set further consists of 49 attributes which can be grouped as
follows (from left to right):

- Columns 1-16 (X1..X16) and 17-32 (Y1..Y16) are positions.
- Columns 33-48 (P1..P16) is the amount of energy absorbed by a Wave Engergy
  Converter (WEC) at a position (e.g. P1 is the energy absorbed at position
  (X1,Y1)).
- Column 49 is the total power output (P). The value for P is the same as the
  sum of P1-P16.

## What types of attributes (nominal, ordinal, interval, ...)


1. WECs positions {X1, X2,.., X16; Y1, Y2,.., Y16} are within a continuous
   range from 0 to 566 (meter).
2. WECs absorbed power: {P1, P2,.., P16}.

The Pk attributes are of type 'ratio', because they have an order, can be
measured and has a unique origin.
The Xk and Yk attributes are of type 'interval', because they are ordered and
can be measured, but their sum/product does not make sense.


## Distribution/histograms of values in the input and target attributes

The plots show histograms for the total power output for each location,
and histograms and distributions for the other attributes. Each group of
attributes is displayed in a separate histogram and distribution plot
respectively. It can be seen that the positions within the locations are
quite similar and that there is a concentration on the extreme ends of
the positions ranges. How the axes relate to each other can not be read
from these plots, therefore we also added kernel density plots to
illustrate their relations. For bervity only two such plots are added
below for the positions in Perth.

### Data visualization for Adelaide

Here included are four plots:

- Power output
- Distribution of positions over their mean values
- Distribution of absorbed power of mean values
- Combined histogram

![Power output histogram, location
Adelaide](images/Adelaide_power_output.png){width="300"}

![Distribution of positions over their mean
values, location Adelaide](images/Adelaide_pos_mean_dist.png){width="300"}

![Distribution of absorbed power of mean
values, location Adelaide](images/Adelaide_power_mean_hist.png){width="300"}

![Combined histogram, location
Adelaide](images/Adelaide_combined_hist.png)

### Data visualization for Perth

Here included are six plots:

- Power output
- Distribution of positions over their mean values
- Distribution of absorbed power of mean values
- Combined histogram
- 2x Distribution of a single position (positions 1 and 14)

The last two plots show the relations between x- and y-axis attributes of the
data set for location Perth.

-   Position 1 with x- and y- coordinates mostly on the extreme, as seen
    in the combined histogram.
-   Position 14 with additional high density areas to the extreme x- and
    y- coordinates.



![Power output histogram, location
Perth](images/Perth_power_output.png){width="300"}

![Distribution of positions over their mean
values, location Perth](images/Perth_pos_mean_dist.png){width="300"}

![Distribution of absorbed power of mean
values, location Perth](images/Perth_power_mean_hist.png){width="300"}

![Combined histogram, location Perth](images/Perth_combined_hist.png)

![Distribution representing combined histogram, location
Perth](images/Perth_location_pos1.png){width="300"}

![Distribution with multiple high density areas, location
Perth](images/Perth_location_pos14.png){width="300"}

### Data visualization for Sydney

Here included are four plots:

- Power output
- Distribution of positions over their mean values
- Distribution of absorbed power of mean values
- Combined histogram

![Power output histogram, location
Sydney](images/Sydney_power_output.png){width="300"}

![Distribution of positions over their mean
values, location Sydney](images/Sydney_pos_mean_dist.png){width="300"}

![Distribution of absorbed power of mean
values, location Sydney](images/Sydney_power_mean_hist.png){width="300"}

![Combined histogram, location Sydney](images/Sydney_combined_hist.png)

### Data visualization for Tasmania

Here included are four plots:

- Power output
- Distribution of positions over their mean values
- Distribution of absorbed power of mean values
- Combined histogram

![Power output histogram, location
Tasmania](images/Tasmania_power_output.png){width="300"}

![Distribution of positions over their mean
values, location Tasmania](images/Tasmania_pos_mean_dist.png){width="300"}

![Distribution of absorbed power of mean
values, location Tasmania](images/Tasmania_power_mean_hist.png){width="300"}

![Combined histogram, location
Tasmania](images/Tasmania_combined_hist.png)

### Relation of poweroutput and locations

To be able to relate the outputs of the different locations this
histogram shows the power output for all locations.

![Power output histogram, location
All](images/All_locations_combined.png){width="300"}


## Numeric values

Values in the Xk and Yk columns range between 0 and 566 (in meter). The values
in Pk and P columns are unbounded by the description. The P values are in fact
between 1M and 5M <!--(presumably in Watt)-->.


### Whether you need to treat these attributes in a pre-processing step

Preprocessing will be necessary for missing values. In addition the values may
have to be rounded before using them further on. It may also be necessary to
sample the data set to cope with the amount of data and avoid too lenghty
expensive calculations further on. Other than that they can be easily processed
further, as there are no string values or problematic attributes.
However one has to make sure, that the Pk attributes are not considered for the
machine learning task.
The reason is that the target P is the sum of these attributes, so the machine
learning task would be trivial and meaningless.
