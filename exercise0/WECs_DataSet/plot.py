#!/usr/bin/python3

import math
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy import stats
from scipy.integrate import trapz
from mpl_toolkits.mplot3d import Axes3D


sns.set(color_codes=True)

location ="../images"

perth = "Perth_Data.csv"
adelaide = "Adelaide_Data.csv"
sydney = "Sydney_Data.csv"
tasmania ="Tasmania_Data.csv"
datasets = [perth,adelaide,sydney,tasmania]

# build header list for dataset according to the data description
column_headers = []

for j in ["X","Y","P"]:
    for i in list(range(1,17)):
        column_headers.append(j+str(i))

column_headers.append("P")


def get_attrlist(attr):
    return [attr+str(i) for i in list(range(1,17))] 

def get_title(dataset):
    return dataset[:dataset.find("_")]

def read_csv(dataset_name):
    location_raw = pd.read_csv(dataset_name,sep=',',names=column_headers)
    #location.applymap(lambda x: x/1000.0)
    #print(round(5.3,0))
    location_rounded = location_raw#.round(decimals=0)
    #print(location.info())
    #print(location.shape)

    location_binned = location_rounded#.applymap(lambda x: x - (x % 10000))

    location = location_binned
    return location


def draw_histogram(dataset_name,title=""):#,figure=None

    if title == "":
        title = get_title(dataset_name)

    print("Processing "+title+" for histogram ...")

    location = read_csv(dataset_name)

    sns.distplot(
        location['P'], kde=False, hist=True, hist_kws={"label":get_title(dataset_name)}
    ).set(xlabel='Total power output',ylabel="count",title=title)



    #sns.pairplot(location)
    #ax = sns.scatterplot(x=location['X1'], y=location['Y1'])
    #ax.set(xticks=np.arange(0,650,100),
    #        yticks=np.arange(0,650,100))

    #sns.regplot(x=location['X1'], y=location['Y1'], fit_reg=False,scatter=True)

    #with sns.axes_style("white"):
    #    for i in list(range(1,3)):
    #        sns.jointplot(x=location['X'+str(i)], y=location['Y'+str(i)], kind="kde")

    #print(location["P"].min())
    #print(location["P"].count())
    #sns.pairplot(location.head());

    #sns.relplot(x="X1", y="Y1", kind="line", data=location)
#    return figure

def draw_joint_kde(dataset_name,title="",cnt=1):

    if title == "":
        title = get_title(dataset_name)

    print("Processing "+str(title)+" for KDE plot...")
    df = read_csv(dataset_name)

    istr = str(cnt)
    print("Creating plot for attribute nr "+istr)

    xaxis = "X"+istr
    yaxis = "Y"+istr
    paxis = "P"+istr

    f1 = sns.jointplot(x=df[xaxis],
                       y=df[yaxis],
                       kind='kde',
                       joint_kws={'label':title}
         )
    f1.set_axis_labels('X'+istr+" (in m)", "Y"+istr+" (in m)")
    #f2 = sns.jointplot(x=df[paxis], y=df["P"], kind='kde',joint_kws={'label':title})

    return f1


def draw_one(func, dataset_name,title="",cnt=None):
    if title == "":
        title = get_title(dataset_name)

    f=plt.figure(dataset_name)
    if cnt is None:
        func(dataset_name,title)
    else:
        f = func(dataset_name,title,cnt)
    return (f,title)

def draw_each(title=""):


    for d in datasets:
        print("Creating histogram plot for "+d+"...")
        f, title = draw_one(draw_histogram, d)

        print("Saving figure...")

        f.savefig(location+"/"+title+"_power_output")

        print("Cleaning up figure...")

        plt.close(f)

#    print("Creating kde plots...")
#
#    datasets = [perth,adelaide,sydney,tasmania]
#    for d in datasets:
#        print("Doing "+d)
#        for i in list(range(1,17)):
#            f_kde, ftitle_kde = draw_one(draw_joint_kde, d,"",i)
#
#            print("Saving figure...")
#            f_kde.savefig(ftitle_kde+"_kde_"+str(i))
#
#            print("Cleaning up figure...")
#            plt.close(f_kde)
    #f.show()
    #g.show()
    #h.show()
    #i.show()

def draw_all(title=""):
    if title == "":
        title = "all_datasets_power_output"
    all=plt.figure(title)
    draw_histogram(perth,title)
    draw_histogram(adelaide,title)
    draw_histogram(sydney,title)
    draw_histogram(tasmania,title)

    plt.legend()
    #all.show()
    all.savefig(location+"/"+title)


def show_plot(func,dataset,title=""):
    f,title = draw_one(func,dataset,title)
    print("showing plot")
    plt.show(f)



#show_plot(draw_joint_kde,"Perth_Data.csv")
def plot_over_position_mean(dataset_name):
    df = read_csv(dataset_name)

    dff = df #df[(np.abs(stats.zscore(df)) < 3).all(axis=1)]
    x = dff["X1"]
    y = dff["Y1"]
    #p = dff["P1"]

    for i in list(range(2,17)):
        istr = str(i)
        x = x + df["X"+istr]
        y = y + df["Y"+istr]
        #p = p + df["P"+istr]

    dfx = pd.DataFrame(x,columns=["X"])
    dfx = dfx.applymap(lambda x: x/16.0)

    dfy = pd.DataFrame(y,columns=["Y"])
    dfy = dfy.applymap(lambda x: x/16.0)

    #dfp1 = pd.DataFrame(p,columns=["P"])
    #dfp = dfp1.applymap(lambda x: x/16.0)

    # dfp1["P"] and df["P"] are equal
    #print(dfp1["P"].head())
    #print(df["P"].head())


    #sns.jointplot(x=dfp["P"],y=df["P"],kind='kde')
    sns.jointplot(x=dfx["X"],y=dfy["Y"],kind='kde')
    #dfxs = dfx.sample(34000)
    #dfys = dfy.sample(34000)
    #plt.plot( dfp["P"],df["P"], linestyle='', marker='o', markersize=0.4)
    plt.show()

#plot_over_position_mean()

def hist_overlay(dataset_name):
    df = read_csv(dataset_name)
    title = get_title(dataset_name)

    for k in ["X","Y","P"]:
        f1=plt.figure(title+"_"+k)
        for i in list(range(1,17)):
            sns.distplot(df[k+str(i)])
        f1.show()

    #f2=plt.figure(title+"_y")
    #for i in list(range(1,17)):
    #    sns.distplot(df["Y"+str(i)],kde=False)
    #f2.show()
#
    #f3=plt.figure(title+"_p")
    #for i in list(range(1,17)):
    #    sns.distplot(df["P"+str(i)],kde=False)
    #f3.show()


    #print((df.mean())["X1"])
    #print((df.mean())["Y1"])
    #sns.jointplot((df.mean())["X1"],(df.mean())["Y1"],kind='kde')
    #plt.show()

def hist_attr16(dataset_name):
    title = get_title(dataset_name)
    df = read_csv(dataset_name)

    for i in list(range(1,17)):
        f1=plt.figure(title+"_x"+str(i))
        sns.distplot(df["X"+str(i)],kde=False)
        f1.show()


    #f2=plt.figure("perth_y")
    #for i in list(range(1,17)):
    #    sns.distplot(df["Y"+str(i)],kde=False)
    #f2.show()
#
    #f3=plt.figure("perth_p")
    #for i in list(range(1,17)):
    #    sns.distplot(df["P"+str(i)],kde=False)
    #f3.show()

    #print((df.mean())["X1"])
    #print((df.mean())["Y1"])
    #sns.jointplot((df.mean())["X1"],(df.mean())["Y1"],kind='kde')
    plt.show()

#hist_attr16()

def distance_plots(dataset_name):
    df = read_csv(dataset_name)

    ### relate power absorbtion / power output to the distance from the origin
    dfpo = pd.DataFrame(df["P"])

    #for i in list(range(1,5)):
    ii = str(15)
    #f1=plt.figure("perth_x"+ii)
    dfx = pd.DataFrame(df["X"+ii])
    dfy = pd.DataFrame(df["Y"+ii])
    dfp = pd.DataFrame(df["P"+ii])

    #sns.kdeplot(df["X1"], df["Y1"], shade=True, color='b')
    #g = sns.jointplot(x="X1", y="Y1", data=df, kind="kde", color="b")
    #g.plot_joint(plt.scatter, c="w", s=3, linewidth=1, marker="+")

    #kdexpo = sns.jointplot(x=df["X1"], y=df["P"], kind='kde')
    #kdeypo = sns.jointplot(x=df["Y1"], y=df["P"], kind='kde')

    dfxsq = dfx.applymap(lambda x: x**2)
    dfysq = dfy.applymap(lambda x: x**2)

    dfn = pd.DataFrame(dfxsq["X"+ii] + dfysq["Y"+ii],columns = ["XY"+ii])
    dfn = dfn.applymap(lambda x: math.sqrt(x))

    #plt.plot(dfn["XY"], linestyle='', marker='o', markersize=0.4)
    #sns.distplot(dfpo["P"])

    sns.jointplot(x=dfn["XY"+ii], y=dfp["P"+ii], kind='scatter', joint_kws={'s':0.4})
    #f1.show()
    plt.show()

#plot_over_position_mean()

### candidate
#for d in datasets:
#    hist_overlay(d)
#plt.show()

def hist_combined_overlay(dataset_name):
    df = read_csv(dataset_name)
    title = get_title(dataset_name)

    # Set up the matplotlib figure
    f, axes = plt.subplots(3, 2, figsize=(20, 20))#,num=title+"_"+k)

    cnt = 0
    label = "distance (in m)"

    for k in ["X","Y","P"]:
        #f1=plt.figure(title+"_"+k)
        if k == "P":
            label = "power output"
        sns.despine(left=True)
        for i in list(range(1,17)):
            n = str(i)
            sns.distplot(df[k+n], kde=False, norm_hist=False, ax=axes[cnt,0], label="N"+n).set(xlabel=k+"k, "+label,ylabel="count",title=title)
            # Plot a kernel density estimate and rug plot
            #sns.distplot(df[k+str(i)], hist=False, rug=True, ax=axes[0, cnt])
            ## Plot a filled kernel density estimate
            sns.distplot(df[k+n], hist=False, kde_kws={"shade": False}, ax=axes[cnt, 1]).set(xlabel=k+"k, "+label,ylabel="density",title=title)
            ## Plot a histogram and kernel density estimate
            #sns.distplot(df[k+str(i)], ax=axes[1, 1])
        cnt += 1

    l = get_attrlist("N")
    f.legend(labels=l,title="N = {X,Y,P}",loc='upper right')
    #axes[0,0].legend(loc='upper center',ncol=2),
    #axes[0,1].legend(loc='upper center',ncol=2)
    #axes[0,2].legend(loc='upper center',ncol=2)

    plt.setp(axes)
    plt.tight_layout()

    f.savefig(location+"/"+title+"_combined_hist")
    #plt.show()

#distance_plots()
#hist_combined_overlay(perth)


def plot_over_mean(dataset_name):

    xn = get_attrlist("X")
    yn = get_attrlist("Y")
    pn = get_attrlist("P")

    title = get_title(dataset_name)

    df = read_csv(dataset_name)

    df_mean_x = (df.mean())[xn]
    df_mean_y = (df.mean())[yn]

    #sns.distplot(df_mean_x, bins=16)
    #sns.distplot(df_mean_y, bins=16)
    f1 = sns.jointplot(x=df_mean_x,
                  y=df_mean_y,
                  kind="reg",
                  joint_kws={'label':title}
     )
    f1.set_axis_labels("X", "Y")
    f1.savefig(location+"/"+title+"_pos_mean_dist")



    f2=plt.figure(title+"_p")
    df_mean_p = (df.mean())[pn]
    sns.distplot(df_mean_p, bins=16)
    f2.savefig(location+"/"+title+"_power_mean_hist")

def main():
    draw_each()
    draw_all("All_locations_combined")
    dataset = pd.DataFrame()
    for d in datasets:
        hist_combined_overlay(d)
        plot_over_mean(d)
        location = read_csv(d)
        dataset = pd.concat([dataset,location],axis=0)


    f=plt.figure("all_locations_power_output")
    sns.distplot(
        dataset['P'], kde=False, hist=True, hist_kws={"label":"all_locations_power_output"}
    ).set(xlabel='Total power output',ylabel="count",title="all_locations_power_output")

    f.savefig("../images/all_locations_power_output")


    ### Print positions kde
    for i in [1,14]:
        f,title = draw_one(draw_joint_kde, perth,title="",cnt=i)
        f.savefig(location+"/"+title+"_location_pos"+str(i))



    


if __name__== "__main__":
    main()
