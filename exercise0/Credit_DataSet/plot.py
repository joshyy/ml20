#!/usr/bin/python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import re
from textwrap import wrap

column_info = {"Status of existing checking account": {"A11": "smaller than 0 DM", "A12": "between 0 (including) and 200 DM",
                                                       "A13": "greater than or equal 200 DM", "A14": "No checking account"},
               "Duration in month": {},
               "Credit history": {"A30": "No credits taken/all credits paid back duly", "A31": "All credits at this bank paid back duly",
                                  "A32": "Existing credits paid back duly till now", "A33": "Delay int paying off in the past",
                                  "A34": "Critical account/ Other credits existing (not at this bank)"},
               "Purpose": {"A40": "Car (new)", "A41": "Car (used)", "A42": "Furniture/ Equipment", "A43": "Radio/ Television", "A44": "Domestic appliances",
                           "A45": "Repairs", "A46": "Education", "A47": "(Vacation - does not exist?)", "A48": "Retraining",
                           "A49": "Business", "A50": "Others"},
               "Credit amount": {},
               "Savings account/bonds": {"A61": "smaller than 100 DM", "A62": "Between 100 (including) and 500", "A63": "Between 500 (including) and 1000",
                                         "A64": "Greater than 1000", "A65": "Unknown/No savings account"},
               "Present employment since": {"A71": "Unemployed", "A72": "Less than 1 year", "A73": "Between 1 (including) and 4 years",
                                            "A74": "Between 4 (including) and 7 yars", "A75": "7 or more years"},
               "Installment rate in percentage of disposable income": {},
               "Personal status and sex": {"A91": "Male: divorced/ spearated", "A92": "Female: divorced/ separated/married", "A93": "Male: single",
                                           "A94": "Male: married/ widowed", "A95": "Female: single"},
               "Other debtors/guarantors": {"A101": "None", "A102": "Co-applicant", "A103": "Guarantor"},
               "Present residence sind": {},
               "Property": {"A121": "Real estate", "A122": "If not real estate: building society savings agreement/ life insurance",
                            "A123": "If not real estate or savings agreement/ life insurance: car or other, not in attribute 6",
                            "A124": "Unknown/ No property"},
               "Age in years": {},
               "Other installment plans": {"A141": "Bank", "A142": "Stores", "A143": "None"},
               "Housing": {"A151": "Rent", "A152": "Own", "A153": "For free"},
               "Number of existing credits at this bank": {},
               "Job": {"A171": "Unemployed/unskilled - non-resident", "A172": "Unskilled - resident", "A173": "Skilled employee/ official",
                       "A174": "Management/ Self-employed/ Highly qualified employee/ officer"},
               "Number of people being liable to provide maintenance for": {},
               "Telephone": {"A191": "None", "A192": "Yes, registered under the customers name"},
               "Foreign worker": {"A201": "Yes", "A202": "No"},
               "Classification": {1: "Good", 2: "Bad"}
               }
file_name = "german.data"
key_whitelist = ("Status of existing checking account", "Duration in month", "Age in years", "Job")

input_data = pd.read_csv(file_name, sep=' ', names=column_info.keys(), index_col=False)
input_data.replace(column_info, inplace=True)
for column_name in column_info:
    if column_name not in key_whitelist:
        continue
    distinct_values = input_data[column_name].unique()
    y_pos = np.arange(len(distinct_values))
    if len(distinct_values) > 7:
        plt.hist(input_data[column_name], bins='auto')
    else:
        value_count = input_data[column_name].value_counts()
        plt.bar(y_pos, value_count)
    if column_info[column_name].__eq__({}):
        labels = None
    else:
        labels = ['\n'.join(wrap(l, 15)) for l in distinct_values]
        plt.xticks(y_pos, labels, fontsize=8)
    plt.ylabel("Number of citizens")
    plt.title(column_name)
    output_file_name = re.sub(r"[ /]", '_', column_name)
    plt.savefig("../mages/"+output_file_name + ".png")
    plt.close()
