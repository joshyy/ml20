#!/usr/bin/python3

import numpy as np
import pandas
import seaborn
import matplotlib.pyplot as pyplot

### Settings ##################################################################
pyplot.rcParams["figure.figsize"] = (10, 6)
seaborn.set(font_scale=2)


### Load dataset ##############################################################
dataset_path = "../datasets/cancer/cancer.lrn.csv"
dataset = pandas.read_csv(dataset_path, sep=",")


### Plotting ##################################################################

# Malignant
ax = seaborn.countplot(x="MALIGNANT", data=dataset)
ax.set(xlabel="Is Malignant", ylabel="Count")
pyplot.tight_layout()
pyplot.savefig("../plots/cancer-malignant.png")
pyplot.clf()

# RadiusMean
seaborn.distplot(dataset["radiusMean"], kde=False, norm_hist=False).set(
        xlabel="Mean radius", ylabel="Count")
pyplot.tight_layout()
pyplot.savefig("../plots/cancer-radiusmean.png")
pyplot.clf()

# RadiusStdErr
seaborn.distplot(dataset["radiusStdErr"], kde=False, norm_hist=False).set(
        xlabel="Standard error of the radius", ylabel="Count")
pyplot.tight_layout()
pyplot.savefig("../plots/cancer-radiusstderr.png")
pyplot.clf()

# RadiusWorst
seaborn.distplot(dataset["radiusWorst"], kde=False, norm_hist=False).set(
        xlabel="Worst radius", ylabel="Count")
pyplot.tight_layout()
pyplot.savefig("../plots/cancer-radiusworst.png")
pyplot.clf()
