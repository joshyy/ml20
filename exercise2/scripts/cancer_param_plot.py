#!/usr/bin/python3

import cancer

import argparse
from ast import literal_eval

import pandas
import seaborn
import matplotlib.pyplot as pyplot
from sklearn import *


def try_parse(s):
    try:
        return literal_eval(s)
    except:
        return s


parser = argparse.ArgumentParser(description="Draw score of a classification method depending on a\
        parameter for the cancer dataset.")
parser.add_argument("-r", action="store", metavar="Reg", default="rf",
        choices=["rf", "knn", "mlp", "ada"],
        help="Select the classifier. Possiblitites are:\
                'rf' (Random Forest, default),\
                'knn' (Nearest Neighbor),\
                'mlp' (Multilayer Perceptron),\
                'ada' (Ada Boost)")
parser.add_argument("--no-scaling", action="store_true")
parser.add_argument("-f", action="store", type=int, metavar="Folds", default=5,
        help="Number of folds")
parser.add_argument("-n", action="store", type=int, metavar="N", default=10,
        help="Number of repetitions")
parser.add_argument("-s", action="store", metavar="Scoring", default="accuracy",
        choices= ['accuracy', 'balanced_accuracy', 'average_precision',
            'neg_brier_score', 'f1', 'f1_micro', 'f1_macro', 'f1_weighted',
            'f1_samples', 'neg_log_loss', 'precision', 'recall', 'jaccard',
            'roc_auc', 'roc_auc_ovr', 'roc_auc_ovo',
            'roc_auc_ovr_weighted', 'roc_auc_ovo_weighted'],
        help="Type of scoring used")

parser.add_argument("-p", action="store", metavar="Path", default=None,
        help="Place to store the plot")
parser.add_argument("-t", action="store", metavar="Title", default="",
        help="Title of the plot")
parser.add_argument("--xscale", action="store", default="linear",
        choices=["linear", "log"],
        help="How to scale the x axis. Possibilites are: 'linear', 'log'")
parser.add_argument("--yscale", action="store", default="linear",
        choices=["linear", "log"],
        help="How to scale the y axis. Possibilites are: 'linear', 'log'")

parser.add_argument("param", action="store",
        help="The parameter, over which will be plotted")
parser.add_argument("values", nargs="+",
        help="A list of values to use for the given parameter")
parser.add_argument("--params", action="append", metavar="id=val", default=[],
        help="A parameter that should be passed to the classifier")


args = parser.parse_args()

classifier_id = args.r
do_scaling = not args.no_scaling
folds = args.f
n_repeats = args.n
scoring = args.s

path = args.p
title = args.t
xscale = args.xscale
yscale = args.yscale
param_id = args.param
param_values = [try_parse(s) for s in args.values]
params = {}
for kv in args.params:
    [key, value] = kv.split("=", 1)
    params[key] = try_parse(value)


### Load dataset ##############################################################
dataset_path = "../datasets/cancer/cancer.lrn.csv"
dataset = pandas.read_csv(dataset_path, sep=",")
dataset = dataset.sample(frac=1);
(features, target, _) = cancer.preprocess(dataset)

### Evaluation ################################################################

if classifier_id == "rf":
    classifier = ensemble.ExtraTreesClassifier
elif classifier_id == "knn":
    classifier = neighbors.KNeighborsClassifier
elif classifier_id == "mlp":
    classifier = neural_network.MLPClassifier
elif classifier_id == "ada":
    classifier = ensemble.AdaBoostClassifier
    params["base_estimator"] = tree.DecisionTreeClassifier(max_leaf_nodes=8, splitter="random", max_features=None)

test_scores = []
train_scores = []
_values = []
for value in param_values:
    model = classifier(**params, **{param_id: value})
    score = model_selection.cross_validate(model, features, y=target, return_train_score=True,
            cv=model_selection.RepeatedStratifiedKFold(n_splits=folds, n_repeats=n_repeats),
            scoring=scoring)
    test_scores += score["test_score"].tolist()
    train_scores += score["train_score"].tolist()
    _values += [value] * folds * n_repeats

if all([type(v) is int or type(v) is float for v in _values]):
    seaborn.set(font_scale=1.5)
    seaborn.lineplot(_values, test_scores, err_style="band")
    axis = seaborn.lineplot(_values, train_scores, err_style="band")
    axis.set(xlabel=param_id, ylabel="Score", title=title)
    axis.set_xscale(xscale)
    axis.set_yscale(yscale)
    pyplot.tight_layout()
    pyplot.legend(["Test scores", "Training scores"])
else:
    seaborn.set(font_scale=1)
    _values = [str(v) for v in _values]
    data = pandas.DataFrame(data = {param_id: _values,
        "Test": test_scores, "Train": train_scores})
    data = pandas.melt(data, id_vars = param_id, var_name = "Train/Test", value_name = "Score")
    axis = seaborn.pointplot(x = param_id, y = "Score", hue = "Train/Test", data = data)
    axis.set(title=title)
    pyplot.tight_layout(rect=[0,0,0.86,1])

if path==None:
    pyplot.show()
else:
    pyplot.savefig(path)
