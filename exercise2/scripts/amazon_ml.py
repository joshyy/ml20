#!/usr/bin/env python3
import datetime

import amazon_reviews

from copy import deepcopy
import argparse
from ast import literal_eval
from statistics import mean, stdev

import pandas
from pandas import DataFrame
from sklearn import *


def try_parse(s):
    try:
        return literal_eval(s)
    except:
        return s


### Parsing ###################################################################
parser = argparse.ArgumentParser(description="Fits a classification model to the\
        car evaluation dataset.")
parser.add_argument("-t", action="store", metavar="Task", default="eval",
                    choices=["eval", "predict"],
                    help="Select the task to perform. Possibilities are:\
                'eval' (cross-validation),\
                'predict' (fit and predict for the unknown data)")
parser.add_argument("-r", action="store", metavar="Clas", default="rf",
                    choices=["rf", "knn", "mlp", "ada"],
                    help="Select the classifier. Possiblitites are:\
                'rf' (Random Forest, default),\
                'knn' (Nearest Neighbor),\
                'mlp' (Multilayer Perceptron),\
                'ada' (Ada Boost)")
parser.add_argument("--no-scaling", action="store_true")
parser.add_argument("-nof", type=int, default=20)
parser.add_argument("--holdout", action="store_true",
                    help="Instead of cross-validation, use simple holdout.\
                     The split is calculated from the sample size and the number of folds.")
parser.add_argument("-f", action="store", type=int, metavar="Folds", default=5,
                    help="Number of folds")
parser.add_argument("-n", action="store", type=int, metavar="N", default=10,
                    help="Number of repetitions")
parser.add_argument("-s", action="append", metavar="Scoring",
                    choices= ['accuracy', 'balanced_accuracy', 'average_precision',
                              'neg_brier_score', 'f1', 'f1_micro', 'f1_macro', 'f1_weighted',
                              'f1_samples', 'neg_log_loss', 'precision', 'precision_weighted', 'recall', 'recall_weighted', 'jaccard',
                              'roc_auc', 'roc_auc_ovr', 'roc_auc_ovo',
                              'roc_auc_ovr_weighted', 'roc_auc_ovo_weighted'],
                    help="Type of scoring used")
parser.add_argument("--params", action="append", metavar="id=val", default=[],
                    help="A parameter that should be passed to the classifier")


args = parser.parse_args()

task = args.t
classifier_id = args.r
do_scaling = not args.no_scaling
folds = args.f
n = args.n
holdout = args.holdout
if args.s != None:
    scoring = args.s
else:
    scoring = ['accuracy']
params = {}
for kv in args.params:
    [key, value] = kv.split("=", 1)
    params[key] = try_parse(value)


### Load dataset ##############################################################
dataset_path = "../datasets/amazon_reviews/amazon_review_ID.shuf.lrn.csv"
dataset = pandas.read_csv(dataset_path, sep=',').sample(frac=1)
preprocessing_arguments = {
    "num_of_features": args.nof
}
(features, target, scaler, feature_ids) = amazon_reviews.preprocess(dataset, preprocessing_arguments, do_scaling = do_scaling)

### Evaluation ################################################################

if classifier_id == "rf":
    classifier = ensemble.ExtraTreesClassifier
elif classifier_id == "knn":
    classifier = neighbors.KNeighborsClassifier
elif classifier_id == "mlp":
    classifier = neural_network.MLPClassifier
elif classifier_id == "ada":
    classifier = ensemble.AdaBoostClassifier
    params["base_estimator"] = tree.DecisionTreeClassifier(max_leaf_nodes=35, splitter="best", max_features=None)

model = classifier(**params)

if task == "eval" and not holdout:
    score = model_selection.cross_validate(model, features, y=target,
            cv=model_selection.RepeatedStratifiedKFold(n_splits=folds, n_repeats=n),
            scoring=scoring)
    for s_id in scoring:
        s = score["test_" + s_id]
        print("%.3f +- %.3f" % (mean(s), stdev(s)))

elif task == "eval" and holdout:
    x_train, x_test, y_train, y_test = \
        model_selection.train_test_split(features, target, test_size=1.0/folds)
    model.fit(x_train,y_train)
    for s_id in scoring:
        scorer = metrics.get_scorer(s_id)
        s = scorer(model, x_test, y_test)
        print("%.3f" % s)

elif task == "predict":
    testset_path = "../datasets/amazon_reviews/amazon_review_ID.shuf.tes.csv"
    testset = pandas.read_csv(testset_path, sep=",")
    ids = testset["ID"]
    testset = scaler.transform(testset[feature_ids])
    times = []
    for i in range(n-1):
        _m = deepcopy(model)
        start_timestamp = datetime.datetime.now()
        _m.fit(features, target)
        res = _m.predict(testset)
        finished_timestamp = datetime.datetime.now()
        times.append(int((finished_timestamp - start_timestamp).total_seconds() * 1000))
    model.fit(features, target)
    res = model.predict(testset)
    DataFrame({"ID": ids, "Class": res}).to_csv("../datasets/amazon_reviews/amazon_review_ID.shuf.sol.csv", index=None)
    #for sid,pred in zip(ids, res):
    #    print("%s,%s" % (sid, str(pred).upper()))
    print(f"time: {int(mean(times))} milliseconds")
