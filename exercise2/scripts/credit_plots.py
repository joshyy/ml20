#!/usr/bin/env python3

import pandas
import seaborn
import matplotlib.pyplot as pyplot

import credit

### Settings ##################################################################

pyplot.rcParams["figure.figsize"] = (10, 6)
seaborn.set(font_scale=1)

dataset = credit.read_dataset(False)
preprocessing_arguments = {
    "num_of_features": 10
}
(features, target, scaler) = credit.preprocess(dataset.copy(), preprocessing_arguments, do_scaling=False)

### Plotting ##################################################################

# heatmap with all features
heatmap_data = pandas.concat([features, target], axis=1)
heatmap = seaborn.heatmap(heatmap_data[heatmap_data.corr().index].corr(), annot=True)
heatmap.set(title=f"Correlation of {len(features.columns)} features and target")
pyplot.tight_layout()
pyplot.savefig("../plots/credit-heatmap.png")
pyplot.clf()

dataset = credit.read_dataset()
(features, target, scaler) = credit.preprocess(dataset.copy(), None, do_scaling=False)

# Class attribute countplot
seaborn.set(font_scale=2)
dataset["Classification"].replace({1: "Good", 2: "Bad"}, inplace=True)
countplot = seaborn.countplot(x="Classification", data=dataset)
pyplot.title("Customer classification")
pyplot.tight_layout()
pyplot.savefig("../plots/credit-class.png")
pyplot.clf()

