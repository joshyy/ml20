#!/usr/bin/python3

from datetime import datetime

import pandas
from sklearn import preprocessing

### Preprocessing of the traffic dataset ######################################
def preprocess(dataset, do_scaling=True):
    dataset = dataset.copy()
    target_id = "MALIGNANT"
    feature_ids = ["radiusMean", "textureMean", "perimeterMean", "areaMean",
            "smoothnessMean", "compactnessMean", "concavityMean",
            "concavePointsMean", "symmetryMean", "fractalDimensionMean",
            "radiusStdErr", "textureStdErr", "perimeterStdErr", "areaStdErr",
            "smoothnessStdErr", "compactnessStdErr", "concavityStdErr",
            "concavePointsStdErr", "symmetryStdErr", "fractalDimensionStdErr",
            "radiusWorst", "textureWorst", "perimeterWorst", "areaWorst",
            "smoothnessWorst", "compactnessWorst", "concavityWorst",
            "concavePointsWorst", "symmetryWorst", "fractalDimensionWorst"]

    # Scaling
    if do_scaling:
        scaler = preprocessing.StandardScaler(with_mean=True, with_std=True, copy=True)
        dataset[feature_ids] = scaler.fit_transform(dataset[feature_ids])
    else:
        scaler = preprocessing.FunctionTransformer()

    restriction_set = []

    # Seperate features and target
    features = dataset.drop(["ID", target_id] + restriction_set, axis=1)
    target = dataset[target_id]

    return (features, target, scaler)
