#!/usr/bin/python3

import cars

from copy import deepcopy
import argparse
from ast import literal_eval
from statistics import mean, stdev

import pandas
import seaborn
import matplotlib.pyplot as pyplot
from sklearn import *


def try_parse(s):
    try:
        return literal_eval(s)
    except:
        return s


### Parsing ###################################################################
parser = argparse.ArgumentParser(description="Fits a classification model to the\
        car evaluation dataset.")
parser.add_argument("-t", action="store", metavar="Task", default="eval",
                    choices=["eval", "predict"],
                    help="Select the task to perform. Possibilities are:\
                'eval' (cross-validation),\
                'predict' (fit and predict for the unknown data)")
parser.add_argument("-r", action="store", metavar="Clas", default="rf",
                    choices=["rf", "knn", "mlp", "ada"],
                    help="Select the classifier. Possiblitites are:\
                'rf' (Random Forest, default),\
                'knn' (Nearest Neighbor),\
                'mlp' (Multilayer Perceptron),\
                'ada' (Ada Boost)")
parser.add_argument("--no-scaling", action="store_true")
parser.add_argument("--holdout", action="store_true",
                    help="Instead of cross-validation, use simple holdout.\
                     The split is calculated from the sample size and the number of folds.")
parser.add_argument("-f", action="store", type=int, metavar="Folds", default=5,
                    help="Number of folds")
parser.add_argument("-n", action="store", type=int, metavar="N", default=10,
                    help="Number of repetitions")
parser.add_argument("-s", action="append", metavar="Scoring",
                    choices=['accuracy', 'balanced_accuracy', 'average_precision',
                             'neg_brier_score', 'f1', 'f1_micro', 'f1_macro', 'f1_weighted',
                             'f1_samples', 'neg_log_loss', 'precision', 'precision_weighted', 'recall',
                             'recall_weighted', 'jaccard', 'roc_auc', 'roc_auc_ovr', 'roc_auc_ovo',
                             'roc_auc_ovr_weighted', 'roc_auc_ovo_weighted'],
                    help="Type of scoring used")
parser.add_argument("--params", action="append", metavar="id=val", default=[],
                    help="A parameter that should be passed to the classifier")


args = parser.parse_args()

task = args.t
classifier_id = args.r
do_scaling = not args.no_scaling
folds = args.f
n = args.n
holdout = args.holdout
if args.s != None:
    scoring = args.s
else:
    scoring = ['accuracy']
params = {}
for kv in args.params:
    [key, value] = kv.split("=", 1)
    params[key] = try_parse(value)


### Load dataset ##############################################################
# dataset_path = "../datasets/cars/car.data"
# dataset = pandas.read_csv(dataset_path, sep=",")
dataset = cars.read_dataset()
dataset = dataset.sample(frac=1)
(features, target, scaler) = cars.preprocess(dataset, do_scaling = do_scaling)

### Evaluation ################################################################

if classifier_id == "rf":
    classifier = ensemble.ExtraTreesClassifier
elif classifier_id == "knn":
    classifier = neighbors.KNeighborsClassifier
elif classifier_id == "mlp":
    classifier = neural_network.MLPClassifier
elif classifier_id == "ada":
    classifier = ensemble.AdaBoostClassifier
    params["base_estimator"] = tree.DecisionTreeClassifier(max_leaf_nodes=35, splitter="best", max_features=None)

model = classifier(**params)

if task == "eval":

    if holdout:
        tes_raw = target.size / folds
        tes = tes_raw / target.size
        trs = (target.size - tes_raw) / target.size
        # print("split: %f:%f" % (tes, trs))

        x_train, x_test, y_train, y_test = model_selection.train_test_split(features, target, test_size=tes, train_size=trs)
        model.fit(x_train,y_train)
        y_pred = model.predict(x_test)
        # score = model.score(x_test,y_test)
        # f1Score = metrics.f1_score(y_test,y_pred,average="macro")
        # print(score)
        # print(f1Score)
        score = None
        for s_id in scoring:
            print(s_id)

            if s_id == 'accuracy':
                score = model.score(x_test,y_test)
            elif s_id == 'precision':
                score = metrics.precision_score(y_test,y_pred)
            elif s_id == 'precision_weighted':
                score = metrics.precision_score(y_test,y_pred,average="weighted",zero_division=0)
            elif s_id == 'recall':
                score = metrics.recall_score(y_test,y_pred)
            elif s_id == 'recall_weighted':
                score = metrics.recall_score(y_test,y_pred,average="weighted",zero_division=0)

            if score is not None:
                print("%.3f" % score)
            score = None

    else:
        score = model_selection.cross_validate(model, features, y=target,
                                               cv=model_selection.RepeatedStratifiedKFold(n_splits=folds, n_repeats=n),
                                               n_jobs=10,
                                               scoring=scoring)
        for s_id in scoring:
            print(s_id)
            s = score["test_" + s_id]
            print("%.3f +- %.3f" % (mean(s), stdev(s)))
elif task == "predict":#
    testset = features
    for i in range(n-1):
        _m = deepcopy(model)
        _m.fit(features, target)
        res = _m.predict(testset)
    model.fit(features, target)
    res = model.predict(testset)
    # print(res)
    for pred in res:
        print("%s" % (str(pred).upper()))

# best params
#rf
# default

#knn
#cars_ml.py -r knn --params n_neighbors=7 --params weights=distance --params leaf_size=100 --params p=1


#mlp
#best:
#cars_ml.py -r mlp --params  learning_rate=invscaling --params power_t=0.00001 --params learning_rate_init=0.1 --params solver=sgd --params max_iter=500

#close second:
#cars_ml.py -r mlp --params  learning_rate=invscaling --params solver=lbfgs --params max_iter=2000


#ada
#cars_ml.py -r ada --params n_estimators=100 --params learning_rate=2 --params algorithm=SAMME --params random_state=13



#scores
#cars_ml.py -f 1 -s accuracy -s balanced_accuracy -s f1_micro -s f1_macro -s f1_weighted -s neg_log_loss -s roc_auc_ovr -s roc_auc_ovo -s roc_auc_ovr_weighted -s roc_auc_ovo_weighted
