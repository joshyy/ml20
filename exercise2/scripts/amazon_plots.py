#!/usr/bin/env python3

import pandas
import seaborn
import matplotlib.pyplot as pyplot

### Settings ##################################################################
import amazon_reviews

pyplot.rcParams["figure.figsize"] = (10, 6)
seaborn.set(font_scale=1)


### Load dataset ##############################################################
dataset_path = "../datasets/amazon_reviews/amazon_review_ID.shuf.lrn.csv"
dataset = pandas.read_csv(dataset_path, sep=",")
(features, target, scaler, feature_ids) = amazon_reviews.preprocess(dataset.copy(), None, do_scaling = False)

### Plotting ##################################################################

# Class attribute countplot
seaborn.countplot(y="Class", data=dataset, order=dataset['Class'].value_counts().index)
pyplot.tight_layout()
pyplot.savefig("../plots/amazon-reviewers.png")
pyplot.clf()

# selected features boxplot
data = features.apply(pandas.value_counts).iloc[[1]]
helper_data = pandas.DataFrame({"Variable": data.columns, "Count": data.values[0]})
seaborn.barplot(x="Variable", y="Count", data=helper_data, order=helper_data.sort_values("Count", ascending=False)["Variable"])
pyplot.title(str(len(data.columns)) + " most important features - true (1) values")
pyplot.tight_layout()
pyplot.savefig("../plots/amazon-explanatories.png")
pyplot.clf()