#!/usr/bin/env python3

# Def.: A _solution_ is a dictionary with parameter names as keys and their
#       values as values.
# Parameters:
#   * getscore: function (solution -> score)
#   * neighbors: function (solution -> [solution]),
#                neighbors(None) gives a single random solution
#   * tabulist: class with following functions
#     - update (solution -> void): adds an element to the tabu-list and,
#         if applicable, removes elements from the tabu-list
#     - is_tabu (solution -> boolean): checks, whether a solution is tabu or not;
#         this does not change the state of the object
#  * time_limit: time in seconds until the function has to return a solution
# Returns: a solution, for whom getscore gives a high score


#####################################################################
# Procedure Tabu-Suche
# begin
#     Initialize tabu list
#     Generate randomly Initial probsolv.tabu.dao.Solution sc
#     Evaluate sc
#     repeat
#         Generate all neighborhood solutions of the solution sc
#         Find best solution sx in the neighborhood
#         if sx is not tabu solution then sc = sx
#         else if 'aspiration criteria' is fulfilled then
#             sc= sx
#         else
#             find best not tabu solution in the neighborhood snt
#             sc= snt
#         Update tabu list
#     until (terminate-condition)
# end
#####################################################################
import time


def all_scores(getscore, neighborhood):
    scores = dict()
    for n in neighborhood:
        score = getscore(n)
        scores[score] = n
    return scores


def highscore(scores, best_score):
    for score in scores.keys():
        if score > best_score:
            best_score = score
    return best_score


# def aspired(tabulist, best):
#     return False

def rank(scores):
    ranked_scores = list(scores.keys())
    ranked_scores.sort(reverse=True)

    return ranked_scores

def tabusearch(getscore, neighbors, tabulist, time_limit, inertia_limit):
    sc = neighbors(None)
    start_time = time.time()

    alltime_high = getscore(sc)
    best = dict()
    cnt = 0

    while True:
        neighborhood = neighbors(sc)

        scores = all_scores(getscore,neighborhood)
        ranked_scores = rank(scores)

        score = ranked_scores.pop(0)
        sx = scores[score]

        best[score] = sx

        if not tabulist.is_tabu(sx):
            sc = sx
        elif score > alltime_high: #aspired(tabulist,sx):
            sc = sx
        else:
            while len(ranked_scores) > 0:
                sx = scores[ranked_scores.pop(0)]
                if not tabulist.is_tabu(sx):
                    sc = sx
                    break

        # for n in neighborhood:
        #     tabulist.update(n)

        tabulist.update(sc)

        if rank(best).pop(0) == alltime_high:
            cnt += 1
        else:
            cnt = 0

        if cnt > inertia_limit:
            print("Inertia limit reached.")
            break

        if (time.time() - start_time) > time_limit:
            print("Timeout reached.")
            break

    return sc
