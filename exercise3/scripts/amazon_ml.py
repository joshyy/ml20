import argparse
from ast import literal_eval

import amazon_reviews
import pandas
from sklearn import *

from tpot import TPOTClassifier
from hpsklearn import HyperoptEstimator, any_classifier, any_preprocessing, any_sparse_classifier
from hyperopt import tpe

def try_parse(s):
    try:
        return literal_eval(s)
    except:
        return s


if __name__ == '__main__':
    ### Parsing ###################################################################
    parser = argparse.ArgumentParser(description="Fits a classification model to the\
            car evaluation dataset.")
    parser.add_argument("-t", action="store", metavar="Task", default="eval",
                        choices=["eval", "predict"],
                        help="Select the task to perform. Possibilities are:\
                    'eval' (cross-validation),\
                    'predict' (fit and predict for the unknown data)")

    parser.add_argument("--no-scaling", action="store_true")
    parser.add_argument("-f", action="store", type=int, metavar="Folds", default=5,
                        help="Number of folds")
    parser.add_argument("-min", action="store", type=int, metavar="Minutes", default=60)
    parser.add_argument("-n", action="store", type=int, metavar="N", default=10,
                        help="Number of repetitions")
    parser.add_argument("-nof", type=int, default=20)
    parser.add_argument("-s", action="append", metavar="Scoring",
                        choices=['accuracy', 'balanced_accuracy', 'average_precision',
                                 'neg_brier_score', 'f1', 'f1_micro', 'f1_macro', 'f1_weighted',
                                 'f1_samples', 'neg_log_loss', 'precision', 'precision_weighted', 'recall',
                                 'recall_weighted', 'jaccard', 'roc_auc', 'roc_auc_ovr', 'roc_auc_ovo',
                                 'roc_auc_ovr_weighted', 'roc_auc_ovo_weighted'],
                        help="Type of scoring used")
    parser.add_argument("--tpot", action="store_true",
                        help="Use tpot library instead of automl.")

    args = parser.parse_args()

    task = args.t
    folds = args.f
    n = args.n
    minutes = args.min
    use_tpot = args.tpot
    if args.s != None:
        scoring = args.s
    else:
        scoring = ['accuracy']

    ### Load dataset ##############################################################
    dataset_path = "../datasets/amazon_reviews/amazon_review_ID.shuf.lrn.csv"
    dataset = pandas.read_csv(dataset_path, sep=',').sample(frac=1)
    preprocessing_arguments = {
        "num_of_features": args.nof
    }
    (features, target, scaler, feature_ids) = amazon_reviews.preprocess(dataset, preprocessing_arguments,
                                                                        do_scaling=use_tpot,
                                                                        do_feature_selection=use_tpot,
                                                                        do_target_encoding=use_tpot)
    x_train, x_test, y_train, y_test = \
        model_selection.train_test_split(features, target, test_size=1.0 / folds)

    ### Evaluation ################################################################
    if use_tpot:
        optimizer = TPOTClassifier(generations=5, population_size=20,
                                   cv=model_selection.RepeatedStratifiedKFold(n_splits=folds, n_repeats=n),
                                   random_state=42, verbosity=0, scoring=scoring, max_time_mins=minutes, n_jobs=-1)
        optimizer.fit(x_train, y_train)
        print(optimizer.fitted_pipeline_)
    else:
        optimizer = HyperoptEstimator(classifier=any_classifier("my_clf"),
                                      preprocessing=any_preprocessing("my_pre"),
                                      algo=tpe.suggest,
                                      max_evals=100,
                                      trial_timeout=60*minutes)
        optimizer.fit(x_train, y_train)
        print(optimizer.best_model())

    for s_id in scoring:
       scorer = metrics.get_scorer(s_id)
       s = scorer(optimizer, x_test, y_test)
       print("%s: %.3f" % (s_id, s))
