#!/usr/bin/env python3
from pandas import DataFrame
from sklearn import preprocessing
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.preprocessing import LabelEncoder

### Preprocessing of the traffic dataset ######################################
def preprocess(dataset, arguments, do_scaling=True, do_feature_selection=True, do_target_encoding=False):
    # Seperate features and target
    target_id = "Class"
    features = dataset.drop(["ID", target_id], axis=1)
    target = dataset[target_id]
    feature_ids = []

    # feature selection
    features = features.loc[:, (features != features.iloc[0]).any()]

    if do_feature_selection:
        try:
            if arguments is None:
                raise KeyError
            feature_num = arguments['num_of_features']
        except KeyError:
            feature_num = 20        

        test = SelectKBest(score_func=f_classif, k=feature_num)
        test.fit(features, target)
        feature_ids = features.columns[test.get_support(indices=True)]
        features = features[feature_ids]

    # Scaling
    if do_scaling:
        scaler = preprocessing.StandardScaler(with_mean=True, with_std=True, copy=True)
        features = scaler.fit_transform(features)
    else:
        scaler = preprocessing.FunctionTransformer()

    if do_target_encoding:
        lb_make = LabelEncoder()
        target = lb_make.fit_transform(target)

    return (features, target, scaler, feature_ids)
