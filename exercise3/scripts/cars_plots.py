#!/usr/bin/python3

import cars

import numpy as np
import pandas
import seaborn
import matplotlib.pyplot as pyplot



### Settings ##################################################################
pyplot.rcParams["figure.figsize"] = (10, 6)
seaborn.set(font_scale=2)

### Load dataset ##############################################################
dataset = cars.read_dataset()


# Class
ax = seaborn.countplot(x="class", data=dataset)
ax.set(xlabel="Class", ylabel="Count")
pyplot.tight_layout()
pyplot.savefig("../plots/car_class.png")
pyplot.clf()

# Heatmap
nDataset, target, _ = cars.preprocess(dataset,False)
fig, axs = pyplot.subplots(3,2,figsize=(20, 20))
i = 0
j = 0
for f in nDataset.columns:
    x = pandas.crosstab([target], nDataset[f],normalize='index')
    seaborn.heatmap(x,annot=True, ax = axs[i,j])
    if j == 1:
        j=0
        i+=1
    else:
        j+=1

pyplot.tight_layout()
pyplot.savefig("../plots/car_heatmap.png")
pyplot.clf()
