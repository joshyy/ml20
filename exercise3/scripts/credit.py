#!/usr/bin/env python3
import pandas
from sklearn import preprocessing
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.preprocessing import LabelEncoder

column_info = {
    "Status of existing checking account": {"A11": "smaller than 0 DM", "A12": "between 0 (including) and 200 DM",
                                            "A13": "greater than or equal 200 DM", "A14": "No checking account"},
    "Duration in month": {},
    "Credit history": {"A30": "No credits taken/all credits paid back duly",
                       "A31": "All credits at this bank paid back duly",
                       "A32": "Existing credits paid back duly till now", "A33": "Delay int paying off in the past",
                       "A34": "Critical account/ Other credits existing (not at this bank)"},
    "Purpose": {"A40": "Car (new)", "A41": "Car (used)", "A42": "Furniture/ Equipment", "A43": "Radio/ Television",
                "A44": "Domestic appliances",
                "A45": "Repairs", "A46": "Education", "A47": "(Vacation - does not exist?)", "A48": "Retraining",
                "A49": "Business", "A50": "Others"},
    "Credit amount": {},
    "Savings account/bonds": {"A61": "smaller than 100 DM", "A62": "Between 100 (including) and 500",
                              "A63": "Between 500 (including) and 1000",
                              "A64": "Greater than 1000", "A65": "Unknown/No savings account"},
    "Present employment since": {"A71": "Unemployed", "A72": "Less than 1 year",
                                 "A73": "Between 1 (including) and 4 years",
                                 "A74": "Between 4 (including) and 7 yars", "A75": "7 or more years"},
    "Installment rate in percentage of disposable income": {},
    "Personal status and sex": {"A91": "Male: divorced/ spearated", "A92": "Female: divorced/ separated/married",
                                "A93": "Male: single",
                                "A94": "Male: married/ widowed", "A95": "Female: single"},
    "Other debtors/guarantors": {"A101": "None", "A102": "Co-applicant", "A103": "Guarantor"},
    "Present residence sind": {},
    "Property": {"A121": "Real estate",
                 "A122": "If not real estate: building society savings agreement/ life insurance",
                 "A123": "If not real estate or savings agreement/ life insurance: car or other, not in attribute 6",
                 "A124": "Unknown/ No property"},
    "Age in years": {},
    "Other installment plans": {"A141": "Bank", "A142": "Stores", "A143": "None"},
    "Housing": {"A151": "Rent", "A152": "Own", "A153": "For free"},
    "Number of existing credits at this bank": {},
    "Job": {"A171": "Unemployed/unskilled - non-resident", "A172": "Unskilled - resident",
            "A173": "Skilled employee/ official",
            "A174": "Management/ Self-employed/ Highly qualified employee/ officer"},
    "Number of people being liable to provide maintenance for": {},
    "Telephone": {"A191": "None", "A192": "Yes, registered under the customers name"},
    "Foreign worker": {"A201": "Yes", "A202": "No"},
    "Classification": {1: "Good", 2: "Bad"}
}

def read_dataset(largeHeaders = True):
    dataset_path = "../datasets/credit/german.data-numeric"
    headers = column_info.keys() if largeHeaders else [str(s) for s in range(21)].append("Class")
    return pandas.read_csv(dataset_path, delimiter=r"\s+", engine="python", header=None, names=headers).sample(frac=1)

### Preprocessing of the traffic dataset ######################################
def preprocess(dataset, arguments, do_scaling=True, do_feature_selection=True):
    # Seperate features and target
    target_id = dataset.columns[-1]
    features = dataset.drop([target_id], axis=1)
    target = dataset[target_id]

    # feature selection
    features = features.loc[:, (features != features.iloc[0]).any()] # replace attributes where all values the same

    if do_feature_selection:
        try:
            if arguments is None:
                raise KeyError
            feature_num = arguments['num_of_features']
        except KeyError:
            feature_num = 5
        test = SelectKBest(score_func=f_classif, k=feature_num)
        test.fit(features, target)
        feature_ids = features.columns[test.get_support(indices=True)]
        features = features[feature_ids]

    # Scaling
    if do_scaling:
        scaler = preprocessing.StandardScaler(with_mean=True, with_std=True, copy=True)
        dataset[feature_ids] = scaler.fit_transform(dataset[feature_ids])
    else:
        scaler = preprocessing.FunctionTransformer()

    lb_make = LabelEncoder()
    target = lb_make.fit_transform(target)

    return (features, target, scaler)
