#!/usr/bin/python3

from datetime import datetime

import pandas
from sklearn import preprocessing


feature_ids = ["buying", "maint", "doors", "persons", "lug_boot", "safety"]
target_id = "class"

def read_dataset():

    headers = feature_ids.copy()
    headers.append(target_id)

    dataset_path = "../datasets/cars/car.data"
    dataset = pandas.read_csv(dataset_path, sep=",", names=headers)

    return dataset

def mapToFloat(dataset):

    mp_buy = {"low": 1, "med": 2, "high": 3, "vhigh": 4}
    mp_maint = {"low": 1, "med": 2, "high": 3, "vhigh": 4}
    mp_doors = {"2": 1, "3": 2, "4": 3, "5more": 4}
    mp_pers = {"2": 1, "4": 2, "more": 3}
    mp_lug = {"small": 1, "med": 2, "big": 3}
    mp_safe = {"low": 1, "med": 2, "high": 3}
    mp_class = {"unacc": 1, "acc": 2, "good": 3, "vgood": 4}

    mp = {"buying": (0,mp_buy), "maint": (1,mp_maint), "doors": (2,mp_doors), "persons": (3,mp_pers),
          "lug_boot": (4,mp_lug), "safety": (5,mp_safe),"class": (6,mp_class)}

    for (k,(idx,mapping)) in mp.items():
        toFloat = dataset[k].map(mapping)
        dsDrop = dataset.drop(k,axis=1)
        dsDrop.insert(idx,k,toFloat)
        dataset = dsDrop

    return dataset

### Preprocessing of the dataset ###
def preprocess(dataset, do_scaling=True):
    dataset = mapToFloat(dataset.copy())

    #Map strings to integers

    # Scaling
    if do_scaling:
        scaler = preprocessing.StandardScaler(with_mean=True, with_std=True, copy=True)
        dataset[feature_ids] = scaler.fit_transform(dataset[feature_ids])
    else:
        scaler = preprocessing.FunctionTransformer()

    restriction_set = []

    # Seperate features and target
    features = dataset.drop([target_id] + restriction_set, axis=1)
    target = dataset[target_id]

    return (features, target, scaler)

