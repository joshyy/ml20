#!/usr/bin/env python3

from statistics import mean
import argparse;
import pandas;
from random import choice;
from sklearn import *;

import amazon_reviews;
import cancer;
import cars;
import credit;

from tabusearch import tabusearch;

### Parsing ###################################################################
parser = argparse.ArgumentParser(description="Finds a good classifier and \
        suitable parameters.");
parser.add_argument("-d", action="store", metavar="Dataset", default="rf",
                    choices=["amazon", "cancer", "cars", "credit"],
                    help="Select the dataset. Possiblitites are:\
                'amazon', 'cancer', 'cars', 'credit'");
parser.add_argument("-t", action="store", type=float, metavar="Time", default=3600,
                    help="Time limit in seconds. Defaults to 3600.");
parser.add_argument("-i", action="store", type=int, metavar="Inertia", default=10,
                    help="Limit for how long the best score cannot change. Defaults to 10 iterations.");
parser.add_argument("-b", action="store", type=int, metavar="TabuIter", default=10,
                    help="Number of iterations, each element stays in the tabu list. \
                Defaults to 10.");
parser.add_argument("-f", action="store", type=int, metavar="Folds", default=5,
                    help="Number of folds. Defaults to 5.");
parser.add_argument("-n", action="store", type=int, metavar="N", default=1,
                    help="Number of repetitions. Defaults to 1.");
parser.add_argument("-s", action="store", metavar="Scoring",
                    choices= ['accuracy', 'balanced_accuracy', 'average_precision',
                              'neg_brier_score', 'f1', 'f1_micro', 'f1_macro', 'f1_weighted',
                              'f1_samples', 'neg_log_loss', 'precision', 'recall', 'jaccard',
                              'roc_auc', 'roc_auc_ovr', 'roc_auc_ovo',
                              'roc_auc_ovr_weighted', 'roc_auc_ovo_weighted'],
                    help="Type of scoring used");

args = parser.parse_args()
dataset = args.d;
time_limit = args.t;
inertia_limit = args.t;
folds = args.f;
n = args.n;
tabu_iters = args.b;
scoring = args.s;


### Load Dataset ##############################################################
features = None
target = None
scaler = None

if dataset == "cancer":
    dataset_path = "../datasets/cancer/cancer.lrn.csv";
    dataset = pandas.read_csv(dataset_path, sep=",");
    dataset = dataset.sample(frac=1);
    (features, target, scaler) = cancer.preprocess(dataset, do_scaling = True);
elif dataset == "amazon":
    dataset_path = "../datasets/amazon_reviews/amazon_review_ID.shuf.lrn.csv";
    dataset = pandas.read_csv(dataset_path, sep=',').sample(frac=1);
    preprocessing_arguments = { "num_of_features": 200 };
    (features, target, scaler, feature_ids) = amazon_reviews.preprocess(dataset,
                                                                        preprocessing_arguments, do_scaling = True);
elif dataset == "cars":
    dataset = cars.read_dataset()
    dataset = dataset.sample(frac=1)
    (features, target, scaler) = cars.preprocess(dataset, do_scaling = True)
elif dataset == "credit":
    (features, target, scaler) = credit.preprocess(credit.read_dataset(), None, do_scaling = True)


### Parameters defining the neighborhood and tabu-list ########################

p_ranges_rf = {"n_estimators": [5, 10, 20, 40, 70, 100, 200, 500],
               "criterion":    ["gini", "entropy"],
               "max_features": [0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 0.8, 0.9, 0.999],
               "bootstrap":    [True, False],
               "max_samples":  [0.05, 0.1, 0.2, 0.5, 0.8, 0.999]};
p_ranges_ada = {"n_estimators": [5, 10, 20, 40, 70, 100, 200, 500],
                "learning_rate": [0.7, 0.9, 0.97, 0.99, 0.997, 1.0]};
p_ranges_svm = {"C": [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30, 100, 300, 1000, 3000],
                "kernel": ["linear", "poly", "rbf", "sigmoid"],
                "degree": [2, 3, 4, 5],
                "gamma": ["scale", "auto"]};
n_hidden_layers = 3;
p_ranges_mlp = {"activation":          ["identity", "logistic", "tanh", "relu"],
                "alpha":               [0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000],
                "max_iter":            [5000],
                # hidden_layers_sizesn = 0 means, that layer n is omitted
                "hidden_layer_sizes0": [0, 5, 10, 20, 50, 100, 200],
                "hidden_layer_sizes1": [0, 5, 10, 20, 50, 100, 200],
                "hidden_layer_sizes2": [0, 5, 10, 20, 50, 100, 200] };
p_ranges_knn = {"n_neighbors":  [1,3,5,7,9,11,13,15,17,19],
                "weights":      ['uniform','distance'],
                # "algorithm":    ['auto', 'ball_tree', 'kd_tree', 'brute'],
                "leaf_size":    [10,20,30,40,50,60,70,80,90],
                "p":            [1,2]};

p_ranges = { "rf": p_ranges_rf, "ada": p_ranges_ada, "svm": p_ranges_svm,
        "mlp": p_ranges_mlp, "knn": p_ranges_knn };

classifiers = { "rf": ensemble.ExtraTreesClassifier,
        "ada": ensemble.AdaBoostClassifier, "svm": svm.SVC,
        "mlp": neural_network.MLPClassifier, "knn": neighbors.KNeighborsClassifier };

def prep_pars_mlp(pars):
    hidden_layer_sizes = ();
    for i in range(n_hidden_layers):
        k = "hidden_layer_sizes" + str(i);
        if pars[k] != 0:
            hidden_layer_sizes += (pars[k],);
    return {"activation": pars["activation"],
            "alpha": pars["alpha"],
            "max_iter": pars["max_iter"],
            "hidden_layer_sizes": hidden_layer_sizes};
prep_pars = { "rf": None, "ada": None, "svm": None, "mlp": prep_pars_mlp, "knn": None };


### Functions #################################################################

# Returns: a new dictionary, where all key-value-pairs are the same as in
#          dictionary, except key key has value value
def change_value(dictionary, key, value):
    ret = dictionary.copy();
    ret[key] = value;
    return ret;


# p_ranges: dictionary with parameters as keys and lists of possible values
#           for these parameters as values
# Returns: a function, that takes a dictionary of parameters with their values
#          and returns a list of similar dictionaries, each with only one
#          parameter value changed
def get_neighbors_fun(p_ranges):
    def neighbors(s):
        if s == None:
            return { p: choice(p_ranges[p]) for p in p_ranges };
        else:
            return [change_value(s, p, v)
                    for p in p_ranges
                    for v in p_ranges[p] if s[p] != v];
    return neighbors;


# an entry of the tabu list
class Tabuentry:
    def __init__(self, par, val, n):
        self.par = par;
        self.val = val;
        self.n = n;
    def __str__(self):
        return str(self.par) + "->" + str(self.val) + "(" + str(self.n) + ")";


# implements a tabulist
# when a parameter is changed, that parameter with the old value gets tabu
class Tabulist:
    # tabu_iters: number of iterations, for which a tabu is kept
    def __init__(self, tabu_iters):
        self.prev_solution = None;
        self.tabu_iters = tabu_iters;
        self.tabus = [];

    def update(self, solution):
        if self.prev_solution != None:
            # remove expired tabus
            for i in range(len(self.tabus)):
                self.tabus[i].n += 1;
            self.tabus = [t for t in self.tabus if t.n < self.tabu_iters];
            # add new tabus
            self.tabus += [Tabuentry(p, self.prev_solution[p], 0)
                    for p in self.prev_solution if self.prev_solution[p] != solution[p]];
        # remember solution
        self.prev_solution = solution;

    def is_tabu(self, solution):
        for t in self.tabus:
            if solution[t.par] == t.val:
                return True;
        return False;


# features, target: data to be fitted
# classifier: the constructor of a classifier
# p_ranges: dictionary with parameters as keys and lists of possible values
#           as values
# folds: number of folds to perform for each evaluation
# n: number of repeated k-folds to perform for each evaluation
# scoring: string, identifying the scoring method used
# tabu_iters: number of iterations, an entry of the tabulist is kept
# time_limit: time in seconds until the function has to return a solution
# Returns: a tuple (parameters, score)
def find_parameters(features, target, classifier, p_ranges, prep_par,
        folds, n, scoring, tabu_iters, time_limit, inertia_limit):
    print("Trying classifier "+str(classifier))
    def get_score(par):
        if prep_par != None:
            par = prep_par(par.copy());
        model = classifier(**par);
        score = model_selection.cross_validate(model, features, y=target,
                cv=model_selection.RepeatedStratifiedKFold(n_splits=folds, n_repeats=n),
                scoring=scoring);
        return mean(score["test_score"]);
    neighbors = get_neighbors_fun(p_ranges);
    tabulist = Tabulist(tabu_iters);
    par = tabusearch(get_score, neighbors, tabulist, time_limit, inertia_limit);
    return (par, get_score(par));


# features, target: data to be fitted
# folds: number of folds to perform for each evaluation
# n: number of repeated k-folds to perform for each evaluation
# scoring: string, identifying the scoring method used
# tabu_iters: number of iterations, an entry of the tabulist is kept
# time_limit: time in seconds until the function has to return a solution
# Returns: a tuple (classifier, classifier_name, dict of parameters, score)
def get_classifier(features, target, folds, n, scoring, tabu_iters, time_limit, inertia_limit):
    pars = {  };
    scores = {  };
    for c in p_ranges:
        (pars[c], scores[c]) = find_parameters(features, target, classifiers[c],
                p_ranges[c], prep_pars[c],
                folds, n, scoring, tabu_iters, time_limit / len(p_ranges), inertia_limit);
    best_classifier = max(scores, key=scores.get);
    par = pars[best_classifier];
    if prep_pars[best_classifier] != None:
        par = prep_pars[best_classifier](par);
    return (classifiers[best_classifier](**par), best_classifier, par, scores[best_classifier]);








### Determine best classifier and parameters ##################################

(cl, cl_name, par, score) = get_classifier(features, target, folds, n,
        scoring, tabu_iters, time_limit, inertia_limit);
print("Classifier: " + cl_name);
print("Parameters: " + str(par));
print("Score: " + str(score));
