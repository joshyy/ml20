def tabusearch(getscore, neighbors, tabulist, time_limit, inertia_limit):
    sc = neighbors(None)
    start_time = time.time()
    alltime_high = getscore(sc)
    best = dict(); cnt = 0

    while cnt <= inertia_limit and (time.time() - start_time) <= time_limit:
        ### Setting variables neighborhood, scores, etc. ###
        if not tabulist.is_tabu(sx):
            sc = sx
        elif score > alltime_high: #aspiration criterium
            sc = sx
        else:
            ### find best non-blocked solution ###
        tabulist.update(sc)
        if rank(best).pop(0) == alltime_high:
            cnt += 1
        else:
            cnt = 0
    return sc
