while cnt <= inertia_limit 
    and (time.time() - start_time) <= time_limit:

    ### Setting variables ###

    if not tabulist.is_tabu(sx):
        sc = sx
    elif score > alltime_high: #aspiration crit
        sc = sx
    else:
        ### find best non-blocked solution ###
    tabulist.update(sc)
    if rank(best).pop(0) == alltime_high:
        cnt += 1
    else:
        cnt = 0
